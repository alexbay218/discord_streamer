import os
import subprocess

videos = set()
playlist = open("playlist.txt","r")
for line in playlist:
    if line == "\n":
        continue
    downloads = open("downloads.txt","r")
    for i in downloads:
        videos.add(i)
    downloads.close()
    ID = line.split('=')
    if ID[1] in videos:
        filestr = "./videos/"+ID[1].rstrip()+".mp4"
        print(filestr)
        os.system("ffmpeg -re -i "+filestr+" -c copy -f flv rtmp://localhost/live/stream > /dev/null 2>&1")
        os.system("echo \"$(tail -n +2 playlist.txt)\" > playlist.txt")
    else:
        print("Video not downloaded")

playlist.close()
