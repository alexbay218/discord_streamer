from __future__ import unicode_literals
import youtube_dl
import ffmpeg
from dotenv import load_dotenv
import os

load_dotenv()
S3 = os.getenv('S3')

def download(): #our function to download videos from the playlist
    videos = set() #creating a set of videos for downloading
    downloads = set() #create of a set of videos that have already been downloaded
    playlist = open("playlist.txt","r+") #open our playlist file
    for line in playlist:
        if line in videos: #if the link is already queued for download skip it
            continue
        else: #else add it to our que
            videos.add(line)
    playlist.close()
    downloaded = open("downloads.txt","r+") #open our text file of already downloaded videos
    for line in downloaded: #add the IDs of those videos to a set
        downloads.add(line)
    downloaded.close()
    ydl_opts = {
        'format': 'bestvideo[vcodec*=avc1]+bestaudio[acodec*=mp4a]',
        'outtmpl': S3+'/videos/%(id)s.%(ext)s'
    }
    downloaded = open("downloads.txt","a+") #open the downloads text file again
    for link in videos: #open our downloads que
        if link == "\n":
            continue
        else:
            splitURL = link.split('=') #grab the ID of the utube video
            ID = splitURL[1]
            if ID not in downloads: #if its already been downloaded, skip it
                try:
                    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                        ydl.download([link])
                    downloaded.write(ID+"\n") #once the video is done downloading, add it to our text file
                except:
                    print(link,"did not work")
