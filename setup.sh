#!/usr/bin/env bash

sudo apt-get update
sudo apt-get --yes --force-yes install build-essential ffmpeg miniupnpc libpcre3 ffcvt libfaac0 libmp3lame0 libpcre3-dev libssl-dev zlib1g-dev wget python3.7 python3-pip 
sudo python3.7 -m pip install --upgrade youtube-dl
sudo python3.7 -m pip install --upgrade discord.py
sudo python3.7 -m pip install --upgrade python-dotenv
sudo python3.7 -m pip install --upgrade asyncio
sudo python3.7 -m pip install --upgrade ffmpeg
sudo rm -rf nginx_build
mkdir nginx_build
cd nginx_build
wget http://nginx.org/download/nginx-1.16.1.tar.gz
wget https://github.com/arut/nginx-rtmp-module/archive/v1.2.1.zip
tar -zxf nginx-1.16.1.tar.gz
unzip v1.2.1.zip
cd nginx-1.16.1
./configure --prefix=/usr/local/nginx --with-http_ssl_module --add-module=../nginx-rtmp-module-1.2.1 --with-cc-opt="-Wimplicit-fallthrough=0"
sudo make -j 1
sudo make install
sudo rm -rf nginx_build
cd ../..
sudo cp index.html /usr/local/nginx/html/
sudo cp nginx.conf /usr/local/nginx/conf/
sudo ufw disable
