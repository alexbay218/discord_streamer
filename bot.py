import os
import shutil
import asyncio
import discord
from discord.ext import commands
from dotenv import load_dotenv
import vodDl

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
URL = os.getenv('URL')
S3 = os.getenv('S3')
#print(TOKEN)
client = commands.Bot(command_prefix = '!')

playlist= open("playlist.txt","a+")

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))
    playlist = 0
    return(playlist)

# @client.event
# async def on_message(message):
#     if message.author == client.user:
#         return
#
#     if message.content.startswith('$hello'):
#         await message.channel.send('Hello!')

@client.command()
async def ping(ctx):
    await ctx.send(f'Pong! {round(client.latency * 1000)}ms')

@client.command()
async def info(ctx):
    await ctx.send("```!ping pings the bot.```")
    await ctx.send("```!url displays the URL of the streaming server.```")
    await ctx.send("```!add [youtube link], adds the youtube link to the playlist and downloads it```")
    await ctx.send("```!play starts streaming the playlist.```")
    await ctx.send("```!pause pauses the stream.```")
    await ctx.send("```!next skips to the next video in the playlist```")
    await ctx.send("```!stop stops the stream.```")
    await ctx.send("```!clear clears the playlist and deletes the videos from the server.```")
    await ctx.send("```!playlist shows the current playlist.```")
    await ctx.send("```!downloads downloads any remaining videos from the playlist and displays the downloaded videos.```")

@client.command()
async def url(ctx):
    global URL
    await ctx.send(URL)

@client.command()
async def add(ctx,link):
    playlist = open("playlist.txt","a+")
    playlist.write(link+"\n")
    playlist.close()
    playlist = open('playlist.txt',"r")
    contents = playlist.read()
    playlist.close()
    await ctx.send('Your link has been added to the playlist.')
    vodDl.download()
    await ctx.send('Your link has been downloaded.')
@client.command()
async def clear(ctx):
    playlist = open("playlist.txt","w+")
    playlist.close()
    downloads = open("downloads.txt","w+")
    downloads.close()
    try:
        shutil.rmtree(S3+"/videos/")
        await ctx.send('Playlist has been cleared.')
    except OSError as e:
        print("Error: %s : %s" % (dir_path, e.strerror))
        await ctx.send('Playlist is already empty.')
    #await ctx.send('Playlist has been cleared.')
@client.command()
async def playlist(ctx):
    playlist = open('playlist.txt',"r+")
    contents = playlist.read()
    playlist.close()
    if(contents == ""):
        await ctx.send("The playlist is empty.")
    else:
        await ctx.send(contents)
@client.command()
async def downloads(ctx):
    vodDl.download()
    downloads = open('downloads.txt',"r+")
    contents = downloads.read()
    if(contents == ""):
        await ctx.send("No videos are downloaded")
    else:
        await ctx.send(contents)
    downloads.close()
@client.command()
async def play(ctx):
    global URL
    commands = open('commands.txt',"w+")
    commands.write("play")
    commands.close()
    message = ('Streaming playlist to '+URL+'.')
    await ctx.send(message)

@client.command()
async def pause(ctx):
    commands = open('commands.txt',"w+")
    commands.write("pause")
    commands.close()
    await ctx.send('Pausing video.')

@client.command()
async def next(ctx):
    commands = open('commands.txt',"w+")
    commands.write("next")
    commands.close()
    await ctx.send('Skipping video.')

@client.command()
async def stop(ctx):
    commands = open('commands.txt',"w+")
    commands.write("stop")
    commands.close()
    await ctx.send('Stopping stream.')



client.run(TOKEN)
