#!/usr/bin/env bash

streampid=0
while [ true ]
do
  if [ -f commands.txt ]
  then
    command=$(cat commands.txt)
    checkstream=$(ps --no-headers -f $streampid | grep python3.7 | wc -l)
    echo "Got command: $command"
    if [[ $command == 'play' ]]
    then
      if [[ $checkstream -gt 0 ]]
      then
        pkill -CONT ffmpeg
      else
        python3.7 streamer.py &
        streampid=$!
      fi
    fi
    if [[ $command == 'stop' ]]
    then
      if [[ $checkstream -gt 0 ]]
      then
        kill -9 $streampid
        pkill -9 ffmpeg
      fi
    fi
    if [[ $command == 'pause' ]]
    then
      if [[ $checkstream -gt 0 ]]
      then
        kill -STOP $streampid
        pkill -STOP ffmpeg
      fi
    fi
    if [[ $command == 'next' ]]
    then
      if [[ $checkstream -gt 0 ]]
      then
        kill -9 $streampid
        pkill -9 ffmpeg
      fi
      echo "$(tail -n +2 playlist.txt)" > playlist.txt
      python3.7 streamer.py &
      streampid=$!
    fi
    echo "Stream Pid: $streampid"
    
    rm -rf commands.txt
  fi
  sleep 1
done